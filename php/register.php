<?php
$user=$_POST['user'];
$password=$_POST['password'];
$email=$_POST['email'];

if($user!=null && $password!=null && $email!=null)
{
	$artistsFileContent = file_get_contents("../artists.json");
	$json=json_decode($artistsFileContent,true);
	$artistsLength=count($json["artists"]);
	$usedUser=false;
	$usedEmail=false;
	for( $i=0;$i<$artistsLength;$i=$i+1)
	{
		$jUser=$json["artists"][$i]["user"];
		$jEmail=$json["artists"][$i]["email"];
		if($user==$jUser)
		{
			$usedUser=true;
			break;
		}
		if($email==$jEmail)
		{
			$usedEmail=true;
			break;
		}
	}
	if($usedEmail)
	{
		echo "usedEmail";
	}
	if($usedUser)
	{
		echo "usedUser";
	}
	if($usedUser==false &&$usedEmail==false)
	{
		$newArtist["user"]=$user;
		$newArtist["password"]=$password;
		$newArtist["email"]=$email;
		array_push($json["artists"],$newArtist);
		$newData = json_encode($json);
		$file=fopen("../artists.json",w);
		fwrite($file, $newData);
		fclose($file);
		mkdir("../artists/".$user);
		echo $user;
	}
}
else 
{
	echo "missingData";
}
?>