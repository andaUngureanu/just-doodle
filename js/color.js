var colors=["black","grey","white","red","orange","yellow","green","blue","indigo","violet"];
var nrOfColors=colors.length;
for(var i=0;i<nrOfColors;i++)
{
	var color=document.createElement('div');
	color.className="color";
	color.style.backgroundColor=colors[i];
	color.addEventListener("click",setColorEffect);
	document.getElementById("colors").appendChild(color);
}
function setColor(color)
{

	context.fillStyle=color;
	context.strokeStyle=color;
	var active= document.getElementsByClassName("active")[0];
	if(active)
	{
		active.className="color";
	}
}
function setColorEffect(e)
{
var colorElement=e.target;
setColor(colorElement.style.backgroundColor);
colorElement.className+=" active";
}
setColorEffect({target:document.getElementsByClassName("color")[0]});