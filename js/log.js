function onLogoutButton(){
	sessionStorage.removeItem("artist");
	modal.style.display="none";
	logged=null;
	deleteCookie("artist");
	while(usernameSpan.firstChild)
	{
		usernameSpan.removeChild(usernameSpan.firstChild);	
	}
	pageContainer.removeChild(paintDiv);
	removeJsFile("js/draw.js");
	removeJsFile("js/color.js");
	removeJsFile("js/resize.js");
	removeJsFile("js/save.js");
	removeJsFile("js/addPaint.js");
	loggedDiv.style.display="none";
	pageContainer.insertBefore(section,footer);
}

function onLoginButton(){
	var artist={
		user:"",
		password:""
	}
	artist.user=loginForm.elements[0].value;
	artist.password=loginForm.elements[1].value;
	var jsonString=JSON.stringify(artist);
	var request=new XMLHttpRequest();
	var url="php/login.php?artist="+jsonString;
	request.onreadystatechange=function()
	{
		if(request.readyState==4 && request.status==200)
		{
			var response=request.responseText;
			if(response=="loginError")
			{
				loginMessage.innerHTML ="Invalid user and password!";
				artist.user="";
				artist.password="";
			}
			if(response=="passwordError")
			{
				loginMessage.innerHTML ="Invalid password!";
				artist.user="";
				artist.password="";	
			}
			if(response!="loginError" && response!="passwordError")
			{
				sessionStorage.setItem("artist",response);
				setCookie("artist",response,1);
				
				pageContainer.removeChild(section);
				usernameSpan.appendChild(document.createTextNode(artist.user));
				loggedDiv.style.display="block";
				//add logout function
				logoutButton.addEventListener("click",onLogoutButton);
				//add canvas and drawing elements
				addJsFile("js/addPaint.js");
			}
		}
		
	}
	request.open("GET",url,true);
	request.send();
}

function onGuestButton(){
	var artist={
		user:"guest",
		password:"paw"
	}
	var jsonString=JSON.stringify(artist);
	var request=new XMLHttpRequest();
	var url="php/login.php?artist="+jsonString;
	request.onreadystatechange=function()
	{
		if(request.readyState==4 && request.status==200)
		{
				var response=request.responseText;
				sessionStorage.setItem("artist",response);
				setCookie("artist",response,1);
				
				pageContainer.removeChild(section);
				usernameSpan.appendChild(document.createTextNode(artist.user));
				loggedDiv.style.display="block";
				//add logout function
				logoutButton.addEventListener("click",onLogoutButton);

				//add canvas and drawing elements
				addJsFile("js/addPaint.js");
				logged=true;
		}
		
	}
	request.open("GET",url,true);
	request.send();
}

//containers
var section=document.getElementsByTagName("section")[0];
var footer=document.getElementsByTagName("footer")[0];
var header=document.getElementsByTagName("header")[0];
var pageContainer=document.getElementsByClassName("page-container")[0];
var activLogin=document.getElementById("activLogin");
var activRegister=document.getElementById("activRegister");

//login elements
var loginDiv=document.getElementById("login");
var loginForm=document.getElementsByClassName("loginForm")[0];
var loginButton=document.getElementById("loginButton");
var guestButton=document.getElementById("guestButton");
var loginMessage=document.getElementById("messageLogin");

//register elements
var registerDiv=document.getElementById("register");
var registerForm=document.getElementsByClassName("registerForm")[0];
var registerButton=document.getElementById("registerButton");
var registerMessage=document.getElementById("messageRegister");

//logout elements
var usernameSpan=document.getElementById("userName");
var logoutButton=document.getElementById("logoutButton");
var galleryButton=document.getElementById("galleryButton");

var loggedDiv=document.getElementById("userLogged");

var logged=sessionStorage.getItem("artist");
//in case of page refresh
if(logged!=null && getCookie("artist")!=0)
{			
	pageContainer.removeChild(section);
	usernameSpan.appendChild(document.createTextNode(sessionStorage.getItem("artist")));
	loggedDiv.style.display="block";
	//add logout function
	logoutButton.addEventListener("click",onLogoutButton);

	//add canvas and drawing elements
	addJsFile("js/addPaint.js");

}

function onLoginTab()
{
	while(loginMessage.firstChild)
	{
		loginMessage.removeChild(loginMessage.firstChild);	
	}
	registerDiv.style.display="none";
	activLogin.setAttribute("class","tab-activ");
	activRegister.setAttribute("class","tab");
	loginDiv.style.display="block";
}

activLogin.addEventListener("click",onLoginTab);
loginButton.addEventListener("click",onLoginButton);
guestButton.addEventListener("click",onGuestButton);