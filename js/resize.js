var minRad=1;
var maxRad=150;
var resizeVal=1;

var radSpan=document.getElementById("radVal"),
	decRadItem=document.getElementById("decreaseRad"),
	incRadItem=document.getElementById("increaseRad");
var setRadius=function(newRadValue)
{
	if(newRadValue<minRad)
		newRadValue=minRad;
	else if(newRadValue>maxRad)
		newRadValue=maxRad;
	radius=newRadValue;
	context.lineWidth=radius*2;
	radSpan.innerHTML=radius;
}
decRadItem.addEventListener("click",function(){
	setRadius(radius-resizeVal);
});
incRadItem.addEventListener("click",function(){
	setRadius(radius+resizeVal);
});