//create elements

//add elements
var paintDiv=document.createElement("div");
paintDiv.setAttribute("id","paint");

var drawElementsDiv=document.createElement("div");
drawElementsDiv.setAttribute("id","drawElements");

var radDiv=document.createElement("div");
radDiv.setAttribute("id","rad");
var radMeasureDiv=document.createElement("div");
radMeasureDiv.setAttribute("id","radMeasure");
radMeasureDiv.setAttribute("class","buttonTab");
radDiv.appendChild(radMeasureDiv);

var spanDiv=document.createElement("span");
spanDiv.setAttribute("id","radVal");
spanDiv.appendChild(document.createTextNode("4"));
radDiv.appendChild(spanDiv);

var decRadDiv=document.createElement("div");
decRadDiv.setAttribute("id","decreaseRad");
decRadDiv.setAttribute("class","radCtrl");
radDiv.appendChild(decRadDiv);

var incRadDiv=document.createElement("div");
incRadDiv.setAttribute("id","increaseRad");
incRadDiv.setAttribute("class","radCtrl");
radDiv.appendChild(incRadDiv);
drawElementsDiv.appendChild(radDiv);//

var colorsDiv=document.createElement("div");
colorsDiv.setAttribute("id","colors");
drawElementsDiv.appendChild(colorsDiv);

var loadDiv=document.createElement("div");
loadDiv.setAttribute("id","load");
loadDiv.setAttribute("class","buttonTab");
drawElementsDiv.appendChild(loadDiv);

var saveDiv=document.createElement("div");
saveDiv.setAttribute("id","save");
saveDiv.setAttribute("class","buttonTab");
drawElementsDiv.appendChild(saveDiv);

var newFileDiv=document.createElement("div");
newFileDiv.setAttribute("id","newFile");
newFileDiv.setAttribute("class","buttonTab");
newFileDiv.addEventListener("click",function(){
	initFillStyle=context.fillStyle;
	context.fillStyle="#FFFFFF";
	context.fillRect(0,0,canvas.width,canvas.height);
	context.fillStyle=initFillStyle;
})
drawElementsDiv.appendChild(newFileDiv);

var rubberDiv=document.createElement("div");
rubberDiv.setAttribute("id","rubber");
rubberDiv.setAttribute("class","buttonTab");
rubberDiv.addEventListener("click",function(){
	context.fillStyle="white";
	context.strokeStyle="white";
})
drawElementsDiv.appendChild(rubberDiv);


paintDiv.appendChild(drawElementsDiv);///

var canvasEl=document.createElement("canvas");
var errCanvas=document.createElement("p");
textErrCanvas="Your browser doesn't support canvas.";
errCanvas.appendChild(document.createTextNode(textErrCanvas));
canvasEl.appendChild(errCanvas);
canvasEl.setAttribute("id","canvas");
paintDiv.appendChild(canvasEl);
pageContainer.insertBefore(paintDiv,footer);

//add popUp Gallery
var modal=document.createElement("div");
modal.setAttribute("class","modal");
var gallery=document.createElement("div");
gallery.setAttribute("id","gallery");
gallery.setAttribute("class","modal-content");
var closeModal=document.createElement("span");
closeModal.appendChild(document.createTextNode("x"));
closeModal.setAttribute("class","close");
gallery.appendChild(closeModal);
var galleryCf=document.createElement("div");
galleryCf.setAttribute("class","gallery cf");
galleryCf.setAttribute("id","gallery-content");
gallery.appendChild(galleryCf);
modal.appendChild(gallery);
pageContainer.appendChild(modal);

//add js files needed
addJsFile("js/draw.js");
addJsFile("js/color.js");
addJsFile("js/resize.js");
addJsFile("js/save.js");
addJsFile("js/gallery.js");
addJsFile("js/load.js");