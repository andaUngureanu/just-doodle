var saveButton=document.getElementById('save');
saveButton.addEventListener("click",saveImage);
function saveImage(){
	var data=canvas.toDataURL();
	var request=new XMLHttpRequest();
	var url="php/save.php";
	request.onreadystatechange=function(){
		if(request.readyState==4 && request.status==200)
		{
			var response=request.responseText;
			window.open(response,"_blank","location=0,menubar=0");
		}
	}
	request.open("POST",url,true);
	request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	request.send("img="+data);
}