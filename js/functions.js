function getCookie(cookieName)
{
	var name=cookieName+"=";
	var cookies=document.cookie.split(";");
	for (var i=0;i<cookies.length;i++)
	{
		var cookie=cookies[i];
		while(cookie.charAt(0) == ' ')
		{
			cookie=cookie.substring(1);
		}
		if(cookie.indexOf(name)==0)
		{
			return cookie.substring(name.length,cookie.length);
		}
		else 
		{	
			return 0;
		}
	}
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function deleteCookie( name ) {
  document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
}
function addJsFile(path){
  var js = document.createElement("script");
  js.type = "text/javascript";
  js.src = path;
  document.body.appendChild(js);
}
function removeJsFile(filename){
    var targetElement= "script" ;
    var targetAttr="src";
    var allSuspects=document.getElementsByTagName("script");
    for (var i=allSuspects.length; i>=0; i--){ 
    if (allSuspects[i] && allSuspects[i].getAttribute("src")!=null && allSuspects[i].getAttribute("src").indexOf(filename)!=-1)
        allSuspects[i].parentNode.removeChild(allSuspects[i]);
    }
}