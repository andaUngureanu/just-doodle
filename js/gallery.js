function onGalleryButton(){
	modal.style.display="block";
	//src to files
	var request=new XMLHttpRequest();
	var url="php/gallery.php";
	request.onreadystatechange=function(){
		if(request.readyState==4 && request.status==200)
		{
			var response=request.responseText;
			var srcFiles=response.split(":");
			var nrOfFiles=srcFiles.length;
			if(response=="artists/"+sessionStorage.getItem("artist")+"/")
			{
				nrOfFiles=0	;
			}
			var divEl=new Array(nrOfFiles);
			var imgEl=new Array(nrOfFiles);
			while(galleryCf.firstChild)
			{
				galleryCf.removeChild(galleryCf.firstChild);	
			}
			for(var i=0;i<nrOfFiles;i=i+1)
			{
				divEl[i]=document.createElement("div");
				imgEl[i]=document.createElement("img");
				imgEl[i].setAttribute("src",srcFiles[i]);
				divEl[i].appendChild(imgEl[i]);
				galleryCf.appendChild(divEl[i]);
			}

		}
	}
	request.open("POST",url,true);
	request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	request.send("artist="+sessionStorage.getItem("artist"));
}
galleryButton.addEventListener("click",onGalleryButton);
function onCloseModal(){
    modal.style.display = "none";
}
closeModal.addEventListener("click",onCloseModal);