loadButton=document.getElementById("load");
function onLoadImage()
{
	console.log(this);
	initialFillStyle=context.fillStyle;
	context.fillStyle="#FFFFFF";
	context.fillRect(0,0,canvas.width,canvas.height);
	context.fillStyle=initialFillStyle;
	context.drawImage(this,0,0);

}
function onLoadButton()
{	
	
	modal.style.display="block";
	//src to files
	var request=new XMLHttpRequest();
	var url="php/gallery.php";
	request.onreadystatechange=function(){
		if(request.readyState==4 && request.status==200)
		{
			var response=request.responseText;
			var srcFiles=response.split(":");
			var nrOfFiles=srcFiles.length;
			if(response=="artists/"+sessionStorage.getItem("artist")+"/")
			{
				nrOfFiles=0	;
			}
			var divEl=new Array(nrOfFiles);
			var imgEl=new Array(nrOfFiles);
			while(galleryCf.firstChild)
			{
				galleryCf.removeChild(galleryCf.firstChild);	
			}
			for(var i=0;i<nrOfFiles;i=i+1)
			{
				divEl[i]=document.createElement("div");
				imgEl[i]=document.createElement("img");
				imgEl[i].setAttribute("src",srcFiles[i]);
				divEl[i].appendChild(imgEl[i]);
				galleryCf.appendChild(divEl[i]);
				imgEl[i].addEventListener("click",onLoadImage);
			}
		}
	}
	request.open("POST",url,true);
	request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	request.send("artist="+sessionStorage.getItem("artist"));
}
loadButton.addEventListener("click",onLoadButton);