var canvas=document.getElementById("canvas");
var context=canvas.getContext("2d");
var radius=4;
var pressed=false;
canvas.width=document.getElementsByClassName("page-container")[0].clientWidth;
canvas.height=document.getElementsByClassName("page-container")[0].clientHeight*1.5;
context.fillStyle="#FFFFFF";
context.fillRect(0,0,canvas.width,canvas.height);
context.fillStyle="#000000";
context.lineWidth=radius*2;
var putPoint=function(e){
	if(pressed){
		context.lineTo(e.offsetX,e.offsetY);
		context.stroke();
		context.beginPath();
		context.arc(e.offsetX,e.offsetY,radius,0,Math.PI*2);
		context.fill();
		context.beginPath();
		context.moveTo(e.offsetX,e.offsetY);
	}
}
var engage=function(e){
	pressed=true;
	putPoint(e);
}
var disengage=function(){
	pressed=false;
	context.beginPath();
}

canvas.addEventListener("mousedown",engage);
canvas.addEventListener("mousemove",putPoint);
canvas.addEventListener("mouseup",disengage);


