function onRegisterTab()
{
	//delete message if it already exists
	while(registerMessage.firstChild)
	{
		registerMessage.removeChild(registerMessage.firstChild);	
	}
	
	loginDiv.style.display="none";
	activLogin.setAttribute("class","tab");
	activRegister.setAttribute("class","tab-activ");
	registerDiv.style.display="block";
}

activRegister.addEventListener("click",onRegisterTab);

function onRegisterButton()
{
	var artist={
		user:"",
		password:"",
		email:""
	}
	artist.user=registerForm.elements[0].value;
	artist.password=registerForm.elements[1].value;
	artist.email=registerForm.elements[2].value;
	data="user="+artist.user+"&password="+artist.password+"&email="+artist.email;
	var request=new XMLHttpRequest();
	var url="php/register.php";
	request.onreadystatechange=function()
	{
		if(request.readyState==4 && request.status==200)
		{
			var response=request.responseText;
			if(response=="missingData")
			{
				while(registerMessage.firstChild)
				{
					registerMessage.removeChild(registerMessage.firstChild);	
				}
				registerMessage.appendChild(document.createTextNode("All fields are required!"));
			}
			if(response=="usedUser")
			{
				while(registerMessage.firstChild)
				{
					registerMessage.removeChild(registerMessage.firstChild);	
				}
				registerMessage.appendChild(document.createTextNode("User already exists!"));
			}
			if(response=="usedEmail")
			{
				while(registerMessage.firstChild)
				{
					registerMessage.removeChild(registerMessage.firstChild);	
				}
				registerMessage.appendChild(document.createTextNode("Email is already used!"));
			}
			if(response !="missingData" && response !="usedUser" && response!="usedEmail")
			{
				onLoginTab();
				while(loginMessage.firstChild)
				{
					loginMessage.removeChild(loginMessage.firstChild);	
				}
				loginMessage.appendChild(document.createTextNode("Registration completed."));
				loginMessage.appendChild(document.createElement("br"));
				loginMessage.appendChild(document.createTextNode("You can log in!"));
			}
			
		}
		
	}
	request.open("POST",url,true);
	request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	request.send(data);
}
registerButton.addEventListener("click",onRegisterButton);